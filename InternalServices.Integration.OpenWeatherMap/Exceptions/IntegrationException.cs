﻿using System;
using WeatherDemo2.Common;

namespace WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Exceptions
{
    public abstract class IntegrationException : WeatherException
    {
        protected IntegrationException(string message) : base(message)
        {
        }

        protected IntegrationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
