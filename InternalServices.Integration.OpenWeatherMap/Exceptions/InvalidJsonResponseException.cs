﻿using System;

namespace WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Exceptions
{
    public class InvalidJsonResponseException : IntegrationException
    {
        public InvalidJsonResponseException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
