﻿using System;

namespace WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Exceptions
{
    public class InvalidHttpResponseException : IntegrationException
    {
        public InvalidHttpResponseException(string message) : base(message)
        {
        }

        public InvalidHttpResponseException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
