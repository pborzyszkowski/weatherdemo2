﻿using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.DataContracts;
using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.ServiceContracts;
using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Exceptions;

namespace WeatherDemo2.InternalServices.Integration.OpenWeatherMap
{
    public class OpenWeatherMapService : IOpenWeatherMapService
    {
        public HttpClient HttpClient { get; }

        public string Endpoint { get; }
        public string AppId { get; }

        public OpenWeatherMapService(HttpClient httpClient, string endpoint, string appId)
        {
            HttpClient = httpClient;
            Endpoint = endpoint;
            AppId = appId;
        }

        public async Task<WeatherResponse> GetWeatherForLocation(string country, string city)
        {
            var url = $"{Endpoint}?q={city},{country}&appid={AppId}";

            var response = await HttpClient.GetAsync(url);
            if (!response.IsSuccessStatusCode)
            {
                throw new InvalidHttpResponseException($"Http call returned {response.StatusCode} for `{url}`");
            }

            try
            {
                var weatherResponse = await response.Content.ReadAsAsync<WeatherResponse>();
                return weatherResponse;
            }
            catch (JsonException ex)
            {
                throw new InvalidJsonResponseException($"Invalid json for `{url}`", ex);
            }
        }
    }
}
