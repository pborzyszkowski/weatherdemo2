﻿using Autofac;
using Autofac.Core;
using WeatherDemo2.Common.Contracts;
using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.ServiceContracts;

namespace WeatherDemo2.InternalServices.Integration.OpenWeatherMap
{
    public static class IocBootstrapper
    {
        public static void Initialize(ContainerBuilder builder)
        {
            builder.RegisterType<OpenWeatherMapService>()
                .As<IOpenWeatherMapService>()
                .WithParameter(new ResolvedParameter(
                    (pi, ctx) => pi.Name == "endpoint",
                    (pi, ctx) => ctx.Resolve<IWeatherConfig>().GetOpenWeatherMapEndpoint
                ))
                .WithParameter(new ResolvedParameter(
                    (pi, ctx) => pi.Name == "appId",
                    (pi, ctx) => ctx.Resolve<IWeatherConfig>().GetOpenWeatherMapAppId
                ))
                ;
        }
    }
}