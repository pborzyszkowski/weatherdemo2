﻿using System.Net;
using System.Net.Http;
using RichardSzalay.MockHttp;
using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Exceptions;
using Xunit;

namespace WeatherDemo2.InternalServices.Integration.OpenWeatherMap.UnitTests
{
    public class OpenWeatherMapServiceUnitTest
    {
        private const string DefaultEndpoint = "https://end.point/api";
        private const string DefaultAppId = "xyz";
        private const string DefaultCountry = "Polska";
        private const string DefaultCity = "Gdansk";

        private const string ResponseJson1 =
            "{\"coord\":{\"lon\":18.69,\"lat\":54.36},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"base\":\"stations\",\"main\":{\"temp\":277.15,\"pressure\":1007,\"humidity\":60,\"temp_min\":277.15,\"temp_max\":277.15},\"visibility\":10000,\"wind\":{\"speed\":10.8,\"deg\":320,\"gust\":16.5},\"clouds\":{\"all\":75},\"dt\":1509364800,\"sys\":{\"type\":1,\"id\":5349,\"message\":0.0334,\"country\":\"PL\",\"sunrise\":1509342243,\"sunset\":1509376367},\"id\":7531002,\"name\":\"Gda\u0144sk\",\"cod\":200}";
        private const string ResponseJson2 =
            "{\"coord\":{\"lon\":-82.46,\"lat\":27.95},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"base\":\"stations\",\"main\":{\"temp\":286.23,\"pressure\":1023,\"humidity\":67,\"temp_min\":283.15,\"temp_max\":290.15},\"visibility\":16093,\"wind\":{\"speed\":2.1,\"deg\":40},\"clouds\":{\"all\":1},\"dt\":1509455700,\"sys\":{\"type\":1,\"id\":726,\"message\":0.003,\"country\":\"US\",\"sunrise\":1509450051,\"sunset\":1509489934},\"id\":4174757,\"name\":\"Tampa\",\"cod\":200}";
        private const string ResponseJson3 =
            "{\"coord\":{\"lon\":150.8,\"lat\":59.57},\"weather\":[{\"id\":621,\"main\":\"Snow\",\"description\":\"shower snow\",\"icon\":\"13n\"}],\"base\":\"stations\",\"main\":{\"temp\":264.15,\"pressure\":984,\"humidity\":72,\"temp_min\":264.15,\"temp_max\":264.15},\"visibility\":1900,\"wind\":{\"speed\":8,\"deg\":350,\"gust\":14},\"clouds\":{\"all\":90},\"dt\":1509454800,\"sys\":{\"type\":1,\"id\":7243,\"message\":0.0034,\"country\":\"RU\",\"sunrise\":1509398118,\"sunset\":1509429868},\"id\":2123628,\"name\":\"Magadan\",\"cod\":200}";

        [Theory]
        [InlineData(500)]
        [InlineData(404)]
        [InlineData(401)]
        public async void GetWeatherForLocation_throws_exception_on_http_error(int errorStatusCode)
        {
            var httpClientMock = CreateHttpClientMock(errorStatusCode, null);
            var sut = CreateSut(httpClientMock);
            await Assert.ThrowsAsync<InvalidHttpResponseException>(() => sut.GetWeatherForLocation(DefaultCountry, DefaultCity));
        }

        [Theory]
        [InlineData("x")]
        [InlineData("{")]
        [InlineData("<div>some error message in html</div>")]
        public async void GetWeatherForLocation_throws_exception_on_invalid_json(string json)
        {
            var httpClientMock = CreateHttpClientMock(null, json);
            var sut = CreateSut(httpClientMock);
            await Assert.ThrowsAsync<InvalidJsonResponseException>(() => sut.GetWeatherForLocation(DefaultCountry, DefaultCity));
        }

        [Theory]
        [InlineData(ResponseJson1, 277.15, 60)]
        [InlineData(ResponseJson2, 286.23, 67)]
        [InlineData(ResponseJson3, 264.15, 72)]
        public async void GetWeatherForLocation_returns_parsed_response(string json, decimal expectedTemperature, int expectedHumidity)
        {
            var httpClientMock = CreateHttpClientMock(null, json);
            var sut = CreateSut(httpClientMock);

            var response = await sut.GetWeatherForLocation(DefaultCountry, DefaultCity);

            Assert.NotNull(response.Main);
            Assert.True(response.Main.Temp.HasValue);
            Assert.True(response.Main.Humidity.HasValue);

            Assert.Equal(expectedTemperature, response.Main.Temp.Value);
            Assert.Equal(expectedHumidity, response.Main.Humidity.Value);
        }

        private OpenWeatherMapService CreateSut(HttpClient httpClientMock, string endpoint = DefaultEndpoint, string appId = DefaultAppId)
        {
            return new OpenWeatherMapService(httpClientMock, endpoint, appId);
        }

        private HttpClient CreateHttpClientMock(int? returnStatusCode, string returnText)
        {
            var mockMessageHandler = new MockHttpMessageHandler();
            if (returnStatusCode.HasValue)
            {
                mockMessageHandler
                    .Fallback
                    .Respond((request) => new HttpResponseMessage((HttpStatusCode)returnStatusCode.Value));
            }
            else
            {
                mockMessageHandler
                    .Fallback
                    .Respond("application/json", returnText);
            }

            var client = mockMessageHandler.ToHttpClient();
            return client;
        }
    }
}
