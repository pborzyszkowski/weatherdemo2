﻿using Newtonsoft.Json;

namespace WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.DataContracts
{
    public class Sys
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("sunrise")]
        public long Sunrise { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("message")]
        public decimal Message { get; set; }

        [JsonProperty("sunset")]
        public long Sunset { get; set; }

        [JsonProperty("type")]
        public long Type { get; set; }
    }
}
