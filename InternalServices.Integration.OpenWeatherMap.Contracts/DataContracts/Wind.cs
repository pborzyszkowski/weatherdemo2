﻿using Newtonsoft.Json;

namespace WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.DataContracts
{
    public class Wind
    {
        [JsonProperty("gust")]
        public decimal Gust { get; set; }

        [JsonProperty("deg")]
        public long Deg { get; set; }

        [JsonProperty("speed")]
        public decimal Speed { get; set; }
    }
}
