﻿using Newtonsoft.Json;

namespace WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.DataContracts
{
    public class Main
    {
        [JsonProperty("pressure")]
        public long? Pressure { get; set; }

        [JsonProperty("temp_max")]
        public decimal? TempMax { get; set; }

        [JsonProperty("humidity")]
        public int? Humidity { get; set; }

        [JsonProperty("temp")]
        public decimal? Temp { get; set; }

        [JsonProperty("temp_min")]
        public decimal? TempMin { get; set; }
    }
}
