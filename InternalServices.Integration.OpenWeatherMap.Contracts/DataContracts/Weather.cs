﻿using Newtonsoft.Json;

namespace WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.DataContracts
{
    public class Weather
    {
        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("main")]
        public string Main { get; set; }
    }
}
