﻿using Newtonsoft.Json;

namespace WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.DataContracts
{
    public class Coord
    {
        [JsonProperty("lat")]
        public decimal Lat { get; set; }

        [JsonProperty("lon")]
        public decimal Lon { get; set; }
    }
}
