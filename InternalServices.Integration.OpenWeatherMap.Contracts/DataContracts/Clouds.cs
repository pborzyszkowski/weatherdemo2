﻿using Newtonsoft.Json;

namespace WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.DataContracts
{
    public class Clouds
    {
        [JsonProperty("all")]
        public long All { get; set; }
    }
}
