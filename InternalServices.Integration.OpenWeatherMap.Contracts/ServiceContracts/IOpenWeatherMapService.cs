﻿using System.Threading.Tasks;
using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.DataContracts;

namespace WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.ServiceContracts
{
    public interface IOpenWeatherMapService
    {
        Task<WeatherResponse> GetWeatherForLocation(string country, string city);
    }
}
