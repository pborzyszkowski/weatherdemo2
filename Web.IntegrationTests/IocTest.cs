﻿using Autofac;
using WeatherDemo2.Web;
using WeatherDemo2.Web.Controllers;
using Xunit;

namespace Web.IntegrationTests
{
    public class IocTest
    {
        [Fact]
        public void IocBootstrapper_registers_controllers()
        {
            var container = IocBootstrapper.Initialize();

            var resolvedWeatherController = container.Resolve<WeatherController>();
            Assert.NotNull(resolvedWeatherController);
        }

    }
}
