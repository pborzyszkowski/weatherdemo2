﻿using System.Net.Http;
using System.Web.Http;
using MvcRouteTester;
using WeatherDemo2.Web;
using WeatherDemo2.Web.Controllers;
using Xunit;

namespace Web.IntegrationTests
{
    public class RoutingTest
    {
        [Fact]
        public void WeatherController_Get_is_routed_properly()
        {
            var config = new HttpConfiguration();

            WebApiConfig.Register(config);
            config.EnsureInitialized();

            config
                .ShouldMap("/api/weather/Poland/Gdańsk")
                .To<WeatherController>(HttpMethod.Get, controller => controller.Get("Poland", "Gdańsk"));
            config
                .ShouldMap("/api/weather/Russia/Magadan")
                .To<WeatherController>(HttpMethod.Get, controller => controller.Get("Russia", "Magadan"));
            config.ShouldMap("/weather/Poland/Gdańsk").ToNoRoute();
            config.ShouldMap("/api/Poland/Gdańsk").ToNoRoute();
            config.ShouldMap("/api/weather/Poland,Gdańsk").ToNoRoute();
        }

        [Fact]
        public void WeatherController_Get_is_routed_only_for_GET_verb()
        {
            var config = new HttpConfiguration();

            WebApiConfig.Register(config);
            config.EnsureInitialized();

            config
                .ShouldMap("/api/weather/Poland/Gdańsk")
                .ToNoMethod<WeatherController>(HttpMethod.Delete);
            config
                .ShouldMap("/api/weather/Poland/Gdańsk")
                .ToNoMethod<WeatherController>(HttpMethod.Head);
            config
                .ShouldMap("/api/weather/Poland/Gdańsk")
                .ToNoMethod<WeatherController>(HttpMethod.Options);
            config
                .ShouldMap("/api/weather/Poland/Gdańsk")
                .ToNoMethod<WeatherController>(HttpMethod.Post);
            config
                .ShouldMap("/api/weather/Poland/Gdańsk")
                .ToNoMethod<WeatherController>(HttpMethod.Put);
            config
                .ShouldMap("/api/weather/Poland/Gdańsk")
                .ToNoMethod<WeatherController>(HttpMethod.Trace);
        }
    }
}
