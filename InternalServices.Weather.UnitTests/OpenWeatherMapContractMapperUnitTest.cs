﻿using System;
using WeatherDemo2.InternalServices.Weather.Contracts.DataContracts;
using WeatherDemo2.InternalServices.Weather.DataMappers;
using Xunit;

namespace WeatherDemo2.InternalServices.Weather.UnitTests
{
    public class OpenWeatherMapContractMapperUnitTest
    {
        [Theory]
        [InlineData(true)]
        [InlineData(false, true)]
        [InlineData(false, false, null, 273.15)]
        [InlineData(false, false, 67, null)]
        public void Map_throws_ArgumentNullException_when_important_data_is_missing_at_integration_layer(
            bool missingResponse = false, bool missingMain = false, double? temp = 0, double? humidity = 0)
        {
            var integrationResponse = Helper.CrateIntegrationResponse(missingResponse, missingMain, temp, humidity);
            var sut = new OpenWeatherMapContractMapper();

            Assert.Throws<ArgumentNullException>(() => sut.Map(integrationResponse));
        }

        [Fact]
        public void Map_returns_mapped_object()
        {
            var integrationResponse = Helper.CrateIntegrationResponse(false, false, 283.15, 100);
            var sut = new OpenWeatherMapContractMapper();

            var weather = sut.Map(integrationResponse);
            Assert.NotNull(weather);
            Assert.Equal(10, weather.Temperature.Value);
            Assert.Equal(TemperatureUnit.Celsius, weather.Temperature.Format);
            Assert.Equal(100, weather.Humidity);
        }
    }
}
