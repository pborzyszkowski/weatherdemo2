﻿using System;
using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.DataContracts;

namespace WeatherDemo2.InternalServices.Weather.UnitTests
{
    internal class Helper
    {
        internal static WeatherResponse CrateIntegrationResponse(bool missingResponse, bool missingMain, double? temp,
            double? humidity)
        {
            WeatherResponse integrationResponse = null;
            if (!missingResponse)
            {
                integrationResponse = new WeatherResponse();
                if (!missingMain)
                {
                    integrationResponse.Main = new Main();
                    if (temp.HasValue)
                    {
                        integrationResponse.Main.Temp = Convert.ToDecimal(temp.Value);
                    }
                    if (humidity.HasValue)
                    {
                        integrationResponse.Main.Humidity = Convert.ToInt32(humidity.Value);
                    }
                }
            }
            return integrationResponse;
        }

    }
}
