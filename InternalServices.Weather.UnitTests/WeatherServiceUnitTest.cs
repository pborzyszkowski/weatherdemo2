﻿using System;
using System.Net.Http;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.DataContracts;
using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.ServiceContracts;
using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Exceptions;
using WeatherDemo2.InternalServices.Weather.Contracts.DataContracts;
using WeatherDemo2.InternalServices.Weather.DataMappers;
using WeatherDemo2.InternalServices.Weather.Exceptions;
using Xunit;

namespace WeatherDemo2.InternalServices.Weather.UnitTests
{
    public class WeatherServiceUnitTest
    {
        private const string DefaultCountry = "Polska";
        private const string DefaultCity = "Gdansk";

        [Theory]
        [InlineData(typeof(InvalidHttpResponseException))]
        [InlineData(typeof(InvalidJsonResponseException))]
        [InlineData(typeof(HttpRequestException))]
        [InlineData(typeof(NullReferenceException))]
        public async void GetWeatherForLocation_wraps_all_exceptions_as_WeatherServiceException(Type exceptionType)
        {
            var thrownException = (Exception)Activator.CreateInstance(exceptionType, string.Empty, new Exception("inner"));
            var integrationService = Substitute.For<IOpenWeatherMapService>();
            integrationService.GetWeatherForLocation(Arg.Any<string>(), Arg.Any<string>()).Throws(thrownException);
            var mapper = CreateMapperSubstitute();

            var sut = new WeatherService(integrationService, mapper);
            await Assert.ThrowsAsync<WeatherServiceException>(() => sut.GetWeatherForLocation(DefaultCountry, DefaultCity));
        }

        [Theory]
        [InlineData(null, null)]
        [InlineData(null, DefaultCity)]
        [InlineData(DefaultCountry, null)]
        public async void GetWeatherForLocation_throws_ArgumentNullException_for_null_params(string country, string city)
        {
            var integrationService = Substitute.For<IOpenWeatherMapService>();
            var mapper = CreateMapperSubstitute();
            var sut = new WeatherService(integrationService, mapper);

            await Assert.ThrowsAsync<ArgumentNullException>(() => sut.GetWeatherForLocation(country, city));
        }

        [Fact]
        public async void GetWeatherForLocation_calls_mapper_and_adds_location_to_its_result()
        {
            Contracts.DataContracts.Weather weather = new Contracts.DataContracts.Weather()
            {
                Humidity = 50,
                Temperature = new Temperature()
                {
                    Format = TemperatureUnit.Celsius,
                    Value = 20,
                }
            };

            var integrationService = CreateWeatherMapServiceSubstitute(out var integrationResponse);
            var mapper = CreateMapperSubstitute();
            mapper.Map(Arg.Any<WeatherResponse>()).Returns(weather);

            var sut = new WeatherService(integrationService, mapper);
            var returnedWeather = await sut.GetWeatherForLocation(DefaultCountry, DefaultCity);

            mapper.Received(1).Map(integrationResponse);
            Assert.NotNull(returnedWeather);
            Assert.NotNull(returnedWeather.Location);
            Assert.Equal(50, returnedWeather.Humidity);
            Assert.Equal(20, returnedWeather.Temperature.Value);
            Assert.Equal(TemperatureUnit.Celsius, returnedWeather.Temperature.Format);
            Assert.Equal(DefaultCountry, returnedWeather.Location.Country);
            Assert.Equal(DefaultCity, returnedWeather.Location.City);
        }

        private static IOpenWeatherMapService CreateWeatherMapServiceSubstitute(out WeatherResponse integrationResponse,
            bool missingResponse = false, bool missingMain = false, double? temp = 0, double? humidity = 0)
        {
            integrationResponse = Helper.CrateIntegrationResponse(missingResponse, missingMain, temp, humidity);
            var integrationService = Substitute.For<IOpenWeatherMapService>();
            integrationService.GetWeatherForLocation(Arg.Any<string>(), Arg.Any<string>()).Returns(integrationResponse);
            return integrationService;
        }

        private IOpenWeatherMapContractMapper CreateMapperSubstitute()
        {
            return Substitute.For<IOpenWeatherMapContractMapper>();
        }
    }
}
