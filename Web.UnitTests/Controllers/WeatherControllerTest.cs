﻿using System;
using NSubstitute;
using WeatherDemo2.InternalServices.Weather.Contracts.DataContracts;
using WeatherDemo2.InternalServices.Weather.Contracts.ServiceContracts;
using WeatherDemo2.Web.Controllers;
using Xunit;

namespace WeatherDemo2.Web.UnitTests.Controllers
{
    public class WeatherControllerTest
    {
        private const string DefaultCountry = "Polska";
        private const string DefaultCity = "Gdansk";

        [Theory]
        [InlineData(null, null)]
        [InlineData(DefaultCountry, null)]
        [InlineData(null, DefaultCity)]
        public async void Get_throws_ArgumentNullException_on_null_params(string country, string city)
        {
            var serviceSubstitute = Substitute.For<IWeatherService>();
            var sut = new WeatherController(serviceSubstitute);

            await Assert.ThrowsAsync<ArgumentNullException>(() => sut.Get(country, city));
        }

        [Fact]
        public async void Get_calls_internal_service_and_bypasses_its_result()
        {
            var result = CreateWeather();

            var serviceSubstitute = Substitute.For<IWeatherService>();
            serviceSubstitute.GetWeatherForLocation(Arg.Any<string>(), Arg.Any<string>()).Returns(result);
            var sut = new WeatherController(serviceSubstitute);

            var receivedWeather = await sut.Get(DefaultCountry, DefaultCity);

            await serviceSubstitute.Received(1).GetWeatherForLocation(DefaultCountry, DefaultCity);
            Assert.Same(result, receivedWeather);
        }

        private Weather CreateWeather()
        {
            var result = new Weather()
            {
                Humidity = 1,
                Temperature = new Temperature()
                {
                    Format = TemperatureUnit.Fahrenheit,
                    Value = 2
                },
                Location = new Location()
                {
                    City = "a",
                    Country = "b"
                }
            };
            return result;
        }
    }
}
