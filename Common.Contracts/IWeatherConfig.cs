﻿namespace WeatherDemo2.Common.Contracts
{
    public interface IWeatherConfig
    {
        string GetOpenWeatherMapEndpoint { get; }

        string GetOpenWeatherMapAppId { get; }
    }
}
