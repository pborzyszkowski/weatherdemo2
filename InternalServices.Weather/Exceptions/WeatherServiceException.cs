﻿using System;
using WeatherDemo2.Common;

namespace WeatherDemo2.InternalServices.Weather.Exceptions
{
    public class WeatherServiceException : WeatherException
    {
        public WeatherServiceException(string message) : base(message)
        {
        }

        public WeatherServiceException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
