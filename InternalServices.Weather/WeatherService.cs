﻿using System;
using System.Threading.Tasks;
using WeatherDemo2.Common;
using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.DataContracts;
using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.ServiceContracts;
using WeatherDemo2.InternalServices.Weather.Contracts.DataContracts;
using WeatherDemo2.InternalServices.Weather.Contracts.ServiceContracts;
using WeatherDemo2.InternalServices.Weather.DataMappers;
using WeatherDemo2.InternalServices.Weather.Exceptions;

namespace WeatherDemo2.InternalServices.Weather
{
    public class WeatherService : IWeatherService
    {
        private readonly IOpenWeatherMapService _openWeatherMapService;
        private readonly IOpenWeatherMapContractMapper _dataMapper;

        public WeatherService(IOpenWeatherMapService openWeatherMapService, IOpenWeatherMapContractMapper dataMapper)
        {
            _openWeatherMapService = openWeatherMapService;
            _dataMapper = dataMapper;
        }

        public async Task<Contracts.DataContracts.Weather> GetWeatherForLocation(string country, string city)
        {
            Ensure.NotNull(() => country);
            Ensure.NotNull(() => city);

            WeatherResponse integrationResponse;
            try
            {
                integrationResponse = await _openWeatherMapService.GetWeatherForLocation(country, city);
            }
            catch (Exception ex)
            {
                throw new WeatherServiceException($"An exception was throws for {country}/{city}", ex);
            }

            var response = _dataMapper.Map(integrationResponse);
            response.Location = new Location()
            {
                Country = country,
                City = city,
            };

            return response;
        }
    }
}
