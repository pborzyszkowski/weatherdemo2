﻿using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.DataContracts;

namespace WeatherDemo2.InternalServices.Weather.DataMappers
{
    public interface IOpenWeatherMapContractMapper
    {
        Contracts.DataContracts.Weather Map(WeatherResponse openWeatherMapResponse);
    }
}
