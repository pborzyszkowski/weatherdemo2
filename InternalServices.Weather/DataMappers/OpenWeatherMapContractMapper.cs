﻿using WeatherDemo2.Common;
using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.DataContracts;
using WeatherDemo2.InternalServices.Weather.Contracts.DataContracts;

namespace WeatherDemo2.InternalServices.Weather.DataMappers
{
    public class OpenWeatherMapContractMapper : IOpenWeatherMapContractMapper
    {
        public Contracts.DataContracts.Weather Map(WeatherResponse openWeatherMapResponse)
        {
            Ensure.NotNull(() => openWeatherMapResponse);
            Ensure.NotNull(() => openWeatherMapResponse.Main);
            Ensure.NotNull(() => openWeatherMapResponse.Main.Temp);
            Ensure.NotNull(() => openWeatherMapResponse.Main.Humidity);

            Ensure.NotLessThan(() => openWeatherMapResponse.Main.Temp.Value, 0);
            Ensure.NotLessThan(() => openWeatherMapResponse.Main.Humidity.Value, 0);
            Ensure.NotMoreThan(() => openWeatherMapResponse.Main.Humidity.Value, 100);

            var mapped = new Contracts.DataContracts.Weather()
            {
                // ReSharper disable once PossibleInvalidOperationException
                Humidity = openWeatherMapResponse.Main.Humidity.Value,
                Temperature = new Temperature()
                {
                    Format = TemperatureUnit.Celsius,
                    // ReSharper disable once PossibleInvalidOperationException
                    Value = ConvertKelvinToCelsius(openWeatherMapResponse.Main.Temp.Value),
                }
            };

            return mapped;
        }

        private decimal ConvertKelvinToCelsius(decimal kelvin)
        {
            return kelvin - 273.15m;
        }
    }
}
