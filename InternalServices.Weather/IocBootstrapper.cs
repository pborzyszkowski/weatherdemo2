﻿using Autofac;
using WeatherDemo2.InternalServices.Weather.Contracts.ServiceContracts;
using WeatherDemo2.InternalServices.Weather.DataMappers;

namespace WeatherDemo2.InternalServices.Weather
{
    public static class IocBootstrapper
    {
        public static void Initialize(ContainerBuilder builder)
        {
            builder.RegisterType<WeatherService>().As<IWeatherService>();
            builder.RegisterType<OpenWeatherMapContractMapper>().As<IOpenWeatherMapContractMapper>();

            Integration.OpenWeatherMap.IocBootstrapper.Initialize(builder);
        }
    }
}
