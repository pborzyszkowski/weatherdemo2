﻿using Autofac;
using WeatherDemo2.InternalServices.Weather.Contracts.ServiceContracts;
using WeatherDemo2.InternalServices.Weather.DataMappers;
using Xunit;

namespace WeatherDemo2.InternalServices.Weather.IntegrationTests
{
    public class IocTest
    {
        [Fact]
        public void IocBootstrapper_registers_services()
        {
            var container = IocBootstrapper.Initialize();

            var resolvedWeatherService = container.Resolve<IWeatherService>();
            Assert.NotNull(resolvedWeatherService);

            var resolvedContractMapper = container.Resolve<IOpenWeatherMapContractMapper>();
            Assert.NotNull(resolvedContractMapper);
        }
    }
}
