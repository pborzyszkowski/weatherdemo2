﻿using System.Configuration;
using System.Net.Http;
using Autofac;
using WeatherDemo2.Common;
using WeatherDemo2.Common.Contracts;

namespace WeatherDemo2.InternalServices.Weather.IntegrationTests
{
    public static class IocBootstrapper
    {
        public static IContainer Initialize()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<HttpClient>().As<HttpClient>();
            builder.RegisterInstance(new WeatherConfig(ConfigurationManager.AppSettings)).As<IWeatherConfig>();
            Weather.IocBootstrapper.Initialize(builder);

            var container = builder.Build();
            return container;
        }
    }
}