﻿using System.Threading.Tasks;
using System.Web.Http;
using WeatherDemo2.Common;
using WeatherDemo2.InternalServices.Weather.Contracts.DataContracts;
using WeatherDemo2.InternalServices.Weather.Contracts.ServiceContracts;

namespace WeatherDemo2.Web.Controllers
{
    [RoutePrefix("api/weather")]
    public class WeatherController : ApiController
    {
        private readonly IWeatherService _weatherService;

        public WeatherController(IWeatherService weatherService)
        {
            _weatherService = weatherService;
        }

        [Route("{country}/{city}")]
        public async Task<Weather> Get(string country, string city)
        {
            Ensure.NotNullOrBlank(() => country);
            Ensure.NotNullOrBlank(() => city);

            var weather = await _weatherService.GetWeatherForLocation(country, city);
            return weather;
        }
    }
}
