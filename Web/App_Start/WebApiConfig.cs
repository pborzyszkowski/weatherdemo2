﻿using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Mvc;
using Autofac.Integration.WebApi;
using WeatherDemo2.Web.Json;

namespace WeatherDemo2.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var container = IocBootstrapper.Initialize();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Services.Replace(typeof(IContentNegotiator), new JsonContentNegotiator(new JsonMediaTypeFormatter()));
        }
    }
}
