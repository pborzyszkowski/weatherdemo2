﻿using System.Configuration;
using System.Net.Http;
using System.Reflection;
using Autofac;
using Autofac.Integration.WebApi;
using WeatherDemo2.Common;
using WeatherDemo2.Common.Contracts;

namespace WeatherDemo2.Web
{
    public static class IocBootstrapper
    {
        public static IContainer Initialize()
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<HttpClient>().As<HttpClient>();
            builder.RegisterInstance(new WeatherConfig(ConfigurationManager.AppSettings)).As<IWeatherConfig>();

            InternalServices.Weather.IocBootstrapper.Initialize(builder);

            var container = builder.Build();
            return container;
        }
    }
}
