﻿using System.Runtime.Serialization;

namespace WeatherDemo2.InternalServices.Weather.Contracts.DataContracts
{
    [DataContract]
    public enum TemperatureUnit
    {
        [EnumMember]
        Celsius,

        [EnumMember]
        Fahrenheit,

        [EnumMember]
        Kelvin,

        [EnumMember]
        Rankine,
    }
}
