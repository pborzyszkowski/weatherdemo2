﻿using System.Runtime.Serialization;

namespace WeatherDemo2.InternalServices.Weather.Contracts.DataContracts
{
    [DataContract]
    public class Location
    {
        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string Country { get; set; }
    }
}