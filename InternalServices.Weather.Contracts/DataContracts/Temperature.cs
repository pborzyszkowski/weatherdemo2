﻿using System.Runtime.Serialization;

namespace WeatherDemo2.InternalServices.Weather.Contracts.DataContracts
{
    [DataContract]
    public class Temperature
    {
        [DataMember]
        public TemperatureUnit Format { get; set; }

        [DataMember]
        public decimal Value { get; set; }
    }
}