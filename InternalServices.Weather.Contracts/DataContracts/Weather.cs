﻿using System.Runtime.Serialization;

namespace WeatherDemo2.InternalServices.Weather.Contracts.DataContracts
{
    [DataContract]
    public class Weather
    {
        [DataMember]
        public Location Location { get; set; }

        [DataMember]
        public Temperature Temperature { get; set; }

        [DataMember]
        public int Humidity { get; set; } 
    }
}