﻿using System.Threading.Tasks;

namespace WeatherDemo2.InternalServices.Weather.Contracts.ServiceContracts
{
    public interface IWeatherService
    {
        Task<DataContracts.Weather> GetWeatherForLocation(string country, string city);
    }
}
