﻿using System;
using System.Runtime.Serialization;

namespace WeatherDemo2.Common
{
    public abstract class WeatherException : Exception
    {
        protected WeatherException()
        {
        }

        protected WeatherException(string message) : base(message)
        {
        }

        protected WeatherException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected WeatherException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
