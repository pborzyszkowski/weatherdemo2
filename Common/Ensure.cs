﻿using System;
using System.Linq.Expressions;

namespace WeatherDemo2.Common
{
    public static class Ensure
    {
        public static void NotNull<T>(Expression<Func<T>> valueExpression)
        {
            if (GetValue(valueExpression) == null)
            {
                var valueName = GetFullMemberName(valueExpression.Body);
                throw new ArgumentNullException(valueName);
            }
        }

        public static void NotNullOrBlank(Expression<Func<string>> valueExpression)
        {
            if (string.IsNullOrWhiteSpace(GetValue(valueExpression)))
            {
                var valueName = GetFullMemberName(valueExpression.Body);
                throw new ArgumentNullException(valueName);
            }
        }

        public static void NotLessThan(Expression<Func<decimal>> valueExpression, decimal limit)
        {
            var value = GetValue(valueExpression);
            if (value < limit)
            {
                var valueName = GetFullMemberName(valueExpression.Body);
                throw new ArgumentException($"{value} should be not less than {limit}", valueName);
            }
        }

        public static void NotMoreThan(Expression<Func<decimal>> valueExpression, decimal limit)
        {
            var value = GetValue(valueExpression);
            if (value > limit)
            {
                var valueName = GetFullMemberName(valueExpression.Body);
                throw new ArgumentException($"{value} should be not more than {limit}", valueName);
            }
        }

        private static T GetValue<T>(Expression<Func<T>> valueExpression)
        {
            return valueExpression.Compile().Invoke();
        }

        private static string GetFullMemberName(Expression valueExpression)
        {
            if (valueExpression is MemberExpression memberExpression)
            {
                var parentName = GetFullMemberName(memberExpression.Expression);
                if (string.IsNullOrEmpty(parentName))
                {
                    return memberExpression.Member.Name;
                }
                else
                {
                    return $"{parentName}.{memberExpression.Member.Name}";
                }
            }
            else
            {
                return null;
            }
        }
    }
}
