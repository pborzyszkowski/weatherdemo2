﻿using System.Collections.Specialized;
using WeatherDemo2.Common.Contracts;

namespace WeatherDemo2.Common
{
    public class WeatherConfig : IWeatherConfig
    {
        public const string OpenWeatherMapEndpointKey = "OpenWeatherMapEndpoint";
        public const string GetOpenWeatherMapAppIdKey = "OpenWeatherMapAppId";

        private readonly NameValueCollection _appSettings;

        public WeatherConfig(NameValueCollection appSettings)
        {
            _appSettings = appSettings;
        }

        public string GetOpenWeatherMapEndpoint => _appSettings[OpenWeatherMapEndpointKey];

        public string GetOpenWeatherMapAppId => _appSettings[GetOpenWeatherMapAppIdKey];
    }
}
