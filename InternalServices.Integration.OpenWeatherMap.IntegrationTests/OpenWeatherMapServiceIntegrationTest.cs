﻿using System;
using System.Configuration;
using Autofac;
using WeatherDemo2.Common;
using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.ServiceContracts;
using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Exceptions;
using Xunit;

namespace WeatherDemo2.InternalServices.Integration.OpenWeatherMap.IntegrationTests
{
    public class OpenWeatherMapServiceUnitTest
    {
        private const string DefaultCountry = "Polska";
        private const string DefaultCity = "Gdansk";

        private static string ProperOpenWeatherMapEndpoint => ConfigurationManager.AppSettings[WeatherConfig.OpenWeatherMapEndpointKey];
        private static string ProperOpenWeatherMapAppId => ConfigurationManager.AppSettings[WeatherConfig.GetOpenWeatherMapAppIdKey];

        [Fact]
        public async void GetWeatherForLocation_throws_generic_exception_when_invalid_endpoint_is_used()
        {
            var container = IocBootstrapper.Initialize();
            var sut = container.Resolve<IOpenWeatherMapService>();

            await Assert.ThrowsAnyAsync<Exception>(() => sut.GetWeatherForLocation(DefaultCountry, DefaultCity));
        }

        [Fact]
        public async void GetWeatherForLocation_throws_exception_when_invalid_appid_is_used()
        {
            var container = IocBootstrapper.Initialize();
            var sut = container.Resolve<IOpenWeatherMapService>(
                new NamedParameter("endpoint", ProperOpenWeatherMapEndpoint)
            );

            await Assert.ThrowsAsync<InvalidHttpResponseException>(() => sut.GetWeatherForLocation(DefaultCountry, DefaultCity));
        }

        [Fact]
        public async void GetWeatherForLocation_returns_forecast_for_correct_endpoint_and_appid()
        {
            var container = IocBootstrapper.Initialize();
            var sut = container.Resolve<IOpenWeatherMapService>(
                new NamedParameter("endpoint", ProperOpenWeatherMapEndpoint),
                new NamedParameter("appId", ProperOpenWeatherMapAppId)
            );

            var weatherResponse = await sut.GetWeatherForLocation(DefaultCountry, DefaultCity);
            Assert.NotNull(weatherResponse);
            Assert.NotNull(weatherResponse.Main);

            Assert.True(weatherResponse.Main.Temp.HasValue);
            Assert.True(weatherResponse.Main.Humidity.HasValue);

            Assert.True(weatherResponse.Main.Temp > 0);
            Assert.True(weatherResponse.Main.Humidity > 0);
            Assert.True(weatherResponse.Main.Humidity <= 100);
        }
    }
}
