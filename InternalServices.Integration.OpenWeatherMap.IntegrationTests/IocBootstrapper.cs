﻿using System.Net.Http;
using Autofac;
using NSubstitute;
using WeatherDemo2.Common.Contracts;

namespace WeatherDemo2.InternalServices.Integration.OpenWeatherMap.IntegrationTests
{
    public static class IocBootstrapper
    {
        public const string TestOpenWeatherMapEndpoint = "https://test.endpoint/api";
        public const string TestOpenWeatherMapAppId = "test_appid";

        public static IContainer Initialize()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<HttpClient>().As<HttpClient>();
            builder.RegisterInstance(CreateWeatherConfig()).As<IWeatherConfig>();

            OpenWeatherMap.IocBootstrapper.Initialize(builder);

            var container = builder.Build();
            return container;
        }

        private static IWeatherConfig CreateWeatherConfig()
        {
            var config = Substitute.For<IWeatherConfig>();
            config.GetOpenWeatherMapEndpoint.Returns(TestOpenWeatherMapEndpoint);
            config.GetOpenWeatherMapAppId.Returns(TestOpenWeatherMapAppId);

            return config;
        }
    }
}