﻿using Autofac;
using WeatherDemo2.InternalServices.Integration.OpenWeatherMap.Contracts.ServiceContracts;
using Xunit;

namespace WeatherDemo2.InternalServices.Integration.OpenWeatherMap.IntegrationTests
{
    public class IocTest
    {
        [Fact]
        public void IocBootstrapper_registers_services()
        {
            var container = IocBootstrapper.Initialize();

            var resolved = container.Resolve<IOpenWeatherMapService>();
            Assert.NotNull(resolved);

            var casted = resolved as OpenWeatherMapService;
            Assert.NotNull(casted);
            Assert.Equal(IocBootstrapper.TestOpenWeatherMapEndpoint, casted.Endpoint);
            Assert.Equal(IocBootstrapper.TestOpenWeatherMapAppId, casted.AppId);
            Assert.NotNull(casted.HttpClient);
        }
    }
}
